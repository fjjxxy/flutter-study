class Api {
  static final String BASE_URL = "https://www.wanandroid.com/";

  static final String MP_WECHAT_NAMES = BASE_URL + "wxarticle/chapters/json";
  static final String LOGIN = BASE_URL + "user/login";
}