import 'package:flutter/material.dart';

class SecondPage extends StatefulWidget {
  final arguments;

  SecondPage({Key key, this.arguments}) : super(key: key);

  @override
  _SecondPageState createState() => _SecondPageState(arguments: this.arguments);
}

class _SecondPageState extends State<SecondPage> {
  Map arguments;

  _SecondPageState({this.arguments});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Content(arguments: arguments,),
      ),
      appBar: AppBar(
        title: Text("第二个页面"),
      ),
    );
  }
}

class Content extends StatelessWidget {
  Map arguments;

  Content({this.arguments});
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("这是第二个页面:id=${arguments!=null&&arguments["id"]!=null?arguments["id"]:"null"}"),
        RaisedButton(
          onPressed: () {
            Navigator.pushNamed(context, "/third");
          },
          child: Text("命名路由跳转第三个页面"),
        )
      ],
    );
  }
}
