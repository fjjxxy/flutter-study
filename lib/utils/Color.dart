import 'package:flutter/material.dart';
class ColorUtils{
  static const  Color color_grey_dd = Color(0xffdddddd);
  static const  Color color_grey_999 = Color(0xff999999);
  static const  Color color_grey_666 = Color(0xff666666);
  static const  Color color_black = Color(0xff000000);
  static const  Color color_white = Color(0xffffffff);
  static const  Color color_godden_dark = Color(0xffb68d45);
  static const  Color color_orange_main = Color(0xffFF791B);
  static const  Color color_blue_EEF6FF = Color(0xffEEF6FF);
  static const  Color color_blue_0C84FF = Color(0xff0C84FF);
  static const  Color color_red_f1091c = Color(0xfff1091c);
}