import 'package:flutter/material.dart';
import 'package:flutter_study/utils/Color.dart';

class CommonDialog extends Dialog {
  var title;
  var titleStyle;
  var content;
  var contentStyle;
  var cancelText;
  var confirmText;
  var onConfirm;
  var onCancel;
  var cancelBtnStyle;
  var confirmBtnStyle;
  var cancelTextStyle;
  var confirmTextStyle;
  var imageNetUrl;
  var imageAssetsUrl;

  CommonDialog(
      {@required this.onConfirm,
      @required this.onCancel,
      this.title = "提示",
      this.content = "",
      this.titleStyle,
      this.contentStyle,
      this.cancelText = "取消",
      this.confirmText = "确定",
      this.cancelBtnStyle,
      this.confirmBtnStyle,
      this.cancelTextStyle,
      this.confirmTextStyle,
      this.imageNetUrl,
      this.imageAssetsUrl})
      : assert(!(imageNetUrl != null && imageAssetsUrl != null));

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Material(
      type: MaterialType.transparency, //透明类型
      child: Center(
        child: Container(
          height: 250,
          width: double.infinity,
          margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
          decoration: BoxDecoration(
            color: ColorUtils.color_white,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: ColorUtils.color_white),
          ),
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                imageAssetsUrl == null
                    ? imageNetUrl != null
                        ? Image.network(
                            imageNetUrl,
                            width: 40,
                            height: 40,
                          )
                        : SizedBox(
                            height: 0,
                          )
                    : Image.asset(
                        imageAssetsUrl,
                        width: 40,
                        height: 40,
                      ),
                imageAssetsUrl != null || imageNetUrl != null
                    ? SizedBox(
                        height: 20,
                      )
                    : SizedBox(
                        height: 0,
                      ),
                Text(
                  "$title",
                  style: this.titleStyle ??
                      TextStyle(fontSize: 20, color: Colors.black),
                ),
                SizedBox(
                  height: 20,
                ),
                Expanded(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                      Expanded(
                          child: Text(
                        "$content",
                        style: this.contentStyle ??
                            TextStyle(fontSize: 15, color: Colors.black),
                      ))
                    ])),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Row(
                    children: [
                      Container(
                        height: 40,
                        width: 100,
                        child: OutlinedButton(
                            onPressed: onCancel,
                            child: Text(
                              "$cancelText",
                              style: cancelTextStyle ?? TextStyle(fontSize: 15),
                            ),
                            style: cancelBtnStyle ??
                                ButtonStyle(
                                    foregroundColor: MaterialStateProperty.all(
                                        ColorUtils.color_blue_0C84FF),
                                    backgroundColor: MaterialStateProperty.all(
                                        ColorUtils.color_blue_EEF6FF),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20))),
                                    side: MaterialStateProperty.all(BorderSide(
                                        color: ColorUtils.color_white)))),
                      ),
                      Expanded(child: Text("")),
                      Container(
                        height: 40,
                        width: 100,
                        child: OutlinedButton(
                            onPressed: onConfirm,
                            child: Text(
                              "$confirmText",
                              style:
                                  confirmTextStyle ?? TextStyle(fontSize: 15),
                            ),
                            style: confirmBtnStyle ??
                                ButtonStyle(
                                    foregroundColor: MaterialStateProperty.all(
                                        ColorUtils.color_white),
                                    backgroundColor: MaterialStateProperty.all(
                                        ColorUtils.color_blue_0C84FF),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20))),
                                    side: MaterialStateProperty.all(BorderSide(
                                        color: ColorUtils.color_white)))),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
