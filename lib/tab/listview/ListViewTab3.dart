import 'package:flutter/material.dart';
import 'package:flutter_study/data/Data.dart';

class ListViewTab3 extends StatefulWidget {
  @override
  _ListViewTab3State createState() => _ListViewTab3State();
}

class _ListViewTab3State extends State<ListViewTab3> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child:  ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: _getData(),
      ),
    );
  }
}

List<Widget> _getData() {
  List<Widget> widgetList = list.map((e){
    return buildContainer(e["name"]);
  }).toList();
  return widgetList;
}

Container buildContainer(String text) {
  return Container(
    alignment: Alignment.center,
    margin: EdgeInsets.all(10),
    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)),border: Border.all(color: Colors.black)),
    width: 100,
    child: ListTile(
      title: Text(text,textAlign: TextAlign.center,),
    ),
  );
}
