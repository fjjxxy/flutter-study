import 'package:flutter/material.dart';
import 'package:flutter_study/utils/Toast.dart';

class TabControllerPage extends StatefulWidget {
  @override
  _TabControllerPageState createState() => _TabControllerPageState();
}

class _TabControllerPageState extends State<TabControllerPage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _tabController.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = new TabController(length: 8, vsync: this);
    _tabController.addListener(() {
      Toast.toast(context, msg: _tabController.index.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("TabControllerPage"),
          bottom: TabBar(
              controller: _tabController,
              unselectedLabelStyle: TextStyle(fontSize: 10),
              labelStyle: TextStyle(fontSize: 20),
              unselectedLabelColor: Colors.white,
              labelColor: Colors.black,
              indicatorWeight: 2,
              //指示器粗细
              indicatorColor: Colors.black,
              //指示器颜色
              isScrollable: true,
              tabs: <Widget>[
                Tab(
                  text: "分类1",
                  icon: Icon(Icons.menu),
                ),
                Tab(
                  text: "分类2",
                ),
                Tab(
                  text: "分类3",
                ),
                Tab(
                  text: "分类4",
                ),
                Tab(
                  text: "分类5",
                ),
                Tab(
                  text: "分类6",
                ),
                Tab(
                  text: "分类7",
                ),
                Tab(
                  text: "分类8",
                ),
              ]),
        ),
        body: TabBarView(controller: _tabController, children: <Widget>[
          Center(
            child: Text("页面1"),
          ),
          Center(
            child: Text("页面2"),
          ),
          Center(
            child: Text("页面3"),
          ),
          Center(
            child: Text("页面4"),
          ),
          Center(
            child: Text("页面5"),
          ),
          Center(
            child: Text("页面6"),
          ),
          Center(
            child: Text("页面7"),
          ),
          Center(
            child: Text("页面8"),
          ),
        ]));
  }
}
