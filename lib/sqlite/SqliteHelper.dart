import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'note_bean_entity.dart';

final String tableNote = 'note';
final String columnId = 'note_id';
final String columnTitle = 'title';
final String columnContent = "content";
final String columnAddTime = "add_time";
final String columnUpdateTime = "update_time";
final String columnNoteCode = "note_code";

///因为增删改查都是异步的原因，所以在执行操作前先确认db是否未空，否则会报错
class SqliteHelper {
  Database db;
  Batch batch;

  Future open() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'note.db');
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          "create table IF NOT EXISTS $tableNote($columnId INTEGER PRIMARY KEY AUTOINCREMENT ,$columnTitle varchar(20)" +
              ",$columnContent TEXT,$columnAddTime DATETIME,$columnUpdateTime DATETIME,$columnNoteCode TEXT)");
    });
  }

  Future<NoteBeanEntity> insert(NoteBeanEntity noteBeanEntity) async {
    Map map = noteBeanEntity.toJson();
    noteBeanEntity.noteId = await db.insert(tableNote, map);
    return noteBeanEntity;
  }

  Future<NoteBeanEntity> getNote(int id) async {
    List<Map> maps = await db.query(tableNote,
        columns: [
          columnId,
          columnTitle,
          columnContent,
          columnAddTime,
          columnUpdateTime,
          columnNoteCode
        ],
        where: "$columnId=?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return NoteBeanEntity().fromJson(maps.first);
    }
    return null;
  }

  /**
   * @param type 1 按编辑日期 2 按创建日期 3 按标题
   */
  Future<List<NoteBeanEntity>> getAllNote(int type) async {
    List<NoteBeanEntity> list = [];
    List<Map> maps;
    switch (type) {
      case 1:
        maps = await db.query(tableNote, orderBy: "$columnUpdateTime desc");

        break;
      case 2:
        maps = await db.query(tableNote, orderBy: "$columnAddTime desc");
        break;
      case 3:
        maps = await db.query(tableNote, orderBy: "$columnTitle asc");
        break;
    }
    maps.map((e) {
      list.add(NoteBeanEntity().fromJson(e));
    }).toList();
    print("Sqlite${maps.length}");
    return list;
  }

  Future<int> delete(int id) async {
    return await db.delete(tableNote, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> deleteAll() async {
    return await db.delete(tableNote);
  }

  Future<int> update(NoteBeanEntity note) async {
    return await db.update(tableNote, note.toJson(),
        where: '$columnId = ?', whereArgs: [note.noteId]);
  }

  Future batchOperate() async {
    batch = db.batch();
    batch.delete(tableNote, where: "$columnTitle=?",whereArgs: ["0"]);
    batch.update(tableNote, {"$columnTitle": 'flutter'},
        where: '$columnTitle = ?', whereArgs: ["3"]);
    List list = await batch.commit();
    print(list.toString());
  }

  Future close() async => db.close();
}
