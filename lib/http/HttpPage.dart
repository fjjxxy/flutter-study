import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_study/http/Api.dart';
import 'package:flutter_study/utils/Toast.dart';
import 'package:http/http.dart' as http;

class HttpPage extends StatefulWidget {
  @override
  _HttpPageState createState() => _HttpPageState();
}

class _HttpPageState extends State<HttpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("网络请求"),
      ),
      body: Container(
        child: ListView(
          children: [
            TextButton(
              onPressed:() async {
                var url = Uri.parse(Api.MP_WECHAT_NAMES);
                var response = await http.get(url,headers: {"token":""});
                Toast.toast(context,
                    msg: jsonDecode(response.body.toString())["errorCode"] == 0
                        ? "请求成功"
                        : "请求失败");
              },
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.red),
                  foregroundColor: MaterialStateProperty.all(Colors.white)),
              child: Text("get请求"),
            ),
            TextButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.red),
                  foregroundColor: MaterialStateProperty.all(Colors.white)),
              onPressed: () async {
                var url = Uri.parse(Api.LOGIN);
                var response = await http.post(url,
                    body: {"username": "xiaohei", "password": "123456"});
                Toast.toast(context,
                    msg: jsonDecode(response.body.toString())["errorCode"] == 0
                        ? "请求成功"
                        : "请求失败");
              },
              child: Text("post请求"),
            )
          ],
        ),
      ),
    );
  }
}
