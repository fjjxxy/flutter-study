import 'package:flutter/material.dart';
import 'package:flutter_study/utils/EventBus.dart';

class PageB extends StatefulWidget {
  @override
  _PageBState createState() => _PageBState();
}

class _PageBState extends State<PageB> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PageB"),
      ),
      body: ElevatedButton(
        child: Text("通知A页面修改文字"),
        onPressed: () {
          EventBus().emit("update", "这是从B界面发送的值");
        },
      ),
    );
  }
}
