import 'package:flutter_study/generated/json/base/json_convert_content.dart';

class WxOfficeBeanEntity with JsonConvert<WxOfficeBeanEntity> {
	List<dynamic> children;
	int courseId;
	int id;
	String name;
	int order;
	int parentChapterId;
	bool userControlSetTop;
	int visible;
}
