import 'package:flutter/material.dart';
import 'package:flutter_study/common/Global.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharePreferencesUtils {
  static SharedPreferences _prefs;

  static Future<SharedPreferences> init() async {
    _prefs = await SharedPreferences.getInstance();
  }

  static SharedPreferences get prefs => _prefs;

  static String getString(key) {
    return prefs.getString(key);
  }

  static Future<String> setString(key, value) async {
    var result;
    await prefs.setString(key, value).then((success) => result = value);
    return result;
  }

  static bool getBool(key) {
    return prefs.getBool(key);
  }

  static Future<bool> setBool(key, value) async {
    return await prefs.setBool(key, value);
  }

  static double getDouble(key) {
    return prefs.getDouble(key);
  }

  static Future<bool> setDouble(key, value) async {
    return await prefs.setDouble(key, value);
  }
}
