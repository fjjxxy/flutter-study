import 'package:flutter/material.dart';
import 'package:flutter_study/data/Data.dart';
import 'package:flutter_study/utils/Toast.dart';

class ListViewTab1 extends StatefulWidget {
  @override
  _ListViewTab1State createState() => _ListViewTab1State();
}

class _ListViewTab1State extends State<ListViewTab1> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _getData(context),
    );
  }
}

List<Widget> _getData(context) {
  List<Widget> widgetList = list.map((value) {
    return ListTile(
      horizontalTitleGap: 10,//前图片与标题，标题与后图片的水平间距，默认16
      contentPadding: EdgeInsets.symmetric(horizontal: 16.0),//类似于组件内的padding，默认只有水平的16间距
      onTap: () {
        Toast.toast(context, msg: "${value["name"]}", position: "bottom");
      },
      dense: true,
      title: Text("${value["name"]}"),
      subtitle: Text("${value["order"]}"),
      leading: Icon(Icons.menu),
    );
  }).toList();
  return widgetList;
}
