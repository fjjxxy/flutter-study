import 'package:flutter/material.dart';
import 'package:flutter_study/component/CommonDialog.dart';
import 'package:flutter_study/utils/Color.dart';
import 'package:flutter_study/utils/Toast.dart';

class DialogPage extends StatefulWidget {
  const DialogPage({Key key}) : super(key: key);

  @override
  _DialogPageState createState() => _DialogPageState();
}

class _DialogPageState extends State<DialogPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dialog"),
      ),
      body: ListView(
        children: [
          ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text("提示"),
                        titlePadding: EdgeInsets.all(20),
                        //标题的内间距
                        titleTextStyle: TextStyle(
                            color: Colors.orange,
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                        content: Text("确定删除吗?删除后数据不可恢复"),
                        //内容的内间距
                        contentPadding: EdgeInsets.all(20),
                        //按钮栏的外间距
                        actionsPadding: EdgeInsets.all(0),
                        contentTextStyle:
                            TextStyle(color: Colors.red, fontSize: 20),
                        //按钮一行放不下的时候就会变成一列，这是每个按钮之间的间距
                        actionsOverflowButtonSpacing: 20,
                        //决定按钮溢出时是从上到下/从下到上排列按钮
                        actionsOverflowDirection: VerticalDirection.up,
                        //按钮栏的内间距
                        buttonPadding: EdgeInsets.all(0),
                        backgroundColor: Colors.white,
                        elevation: 10,
                        //当内容和标题溢出时是否滚动，滚动时标题和内容会一起滚动
                        scrollable: true,
                        actions: [
                          TextButton(
                              onPressed: () {
                                Toast.toast(context, msg: "确定");
                              },
                              child: Text("确定")),
                          TextButton(
                              onPressed: () {
                                Toast.toast(context, msg: "取消");
                              },
                              child: Text("取消")),
                          TextButton(
                              onPressed: () {
                                Toast.toast(context, msg: "取消");
                              },
                              child: Text("取消")),
                          TextButton(
                              onPressed: () {
                                Toast.toast(context, msg: "取消");
                              },
                              child: Text("取消"))
                        ],
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.red, width: 1),
                            borderRadius: BorderRadius.circular(20)),
                      );
                    });
              },
              child: Text("AlertDialog")),
          ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return SimpleDialog(
                        title: Text("提示"),
                        children: [
                          SimpleDialogOption(
                            onPressed: () {
                              Toast.toast(context, msg: "A");
                            },
                            child: Text("选项A"),
                          ),
                          SimpleDialogOption(
                            onPressed: () {
                              Toast.toast(context, msg: "B");
                            },
                            child: Text("选项B"),
                          ),
                          SimpleDialogOption(
                            onPressed: () {
                              Toast.toast(context, msg: "C");
                            },
                            child: Text("选项C"),
                          )
                        ],
                      );
                    });
              },
              child: Text("SimpleDialog")),
          ElevatedButton(
              onPressed: () {
                showModalBottomSheetFunc(context);
              },
              child: Text("BottomSheet")),
          ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return CommonDialog(
                        title: "温馨提示",
                        titleStyle:
                            TextStyle(color: Colors.black, fontSize: 20),
                        content: "公共场所请勿抽烟",
                        contentStyle:
                            TextStyle(color: Colors.black, fontSize: 15),
                        confirmText: "下一步",
                        cancelText: "取消",
                        onCancel: () {
                          Toast.toast(context, msg: "取消");
                          Navigator.pop(context);
                        },
                        onConfirm: () {
                          Toast.toast(context, msg: "确定");
                          Navigator.pop(context);
                        },
                        cancelBtnStyle: ButtonStyle(
                            foregroundColor: MaterialStateProperty.all(
                                ColorUtils.color_blue_0C84FF),
                            backgroundColor: MaterialStateProperty.all(
                                ColorUtils.color_blue_EEF6FF),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20))),
                            side: MaterialStateProperty.all(
                                BorderSide(color: ColorUtils.color_white))),
                        confirmBtnStyle:                                 ButtonStyle(
                            foregroundColor: MaterialStateProperty.all(
                                ColorUtils.color_white),
                            backgroundColor: MaterialStateProperty.all(
                                ColorUtils.color_blue_0C84FF),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius:
                                    BorderRadius.circular(20))),
                            side: MaterialStateProperty.all(BorderSide(
                                color: ColorUtils.color_white))),
                        cancelTextStyle: TextStyle(fontSize: 15),
                        confirmTextStyle: TextStyle(fontSize: 15),
                        // imageNetUrl: "https://profile.csdnimg.cn/D/6/7/0_qq_40785165",
                        imageAssetsUrl: "assets/images/icon_warn.png",
                      );
                    });
              },
              child: Text("自定义Dialog"))
        ],
      ),
    );
  }

  void showModalBottomSheetFunc(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: 200,
            child: ListView.separated(
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: Icon(Icons.radio_button_off),
                    title: Text("A"),
                  );
                },
                separatorBuilder: (context, index) {
                  return Container(
                    height: 1,
                    color: Colors.grey,
                  );
                },
                itemCount: 10),
          );
        });
  }
}
