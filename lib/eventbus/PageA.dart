import 'package:flutter/material.dart';
import 'package:flutter_study/utils/EventBus.dart';

class PageA extends StatefulWidget {
  const PageA({Key key}) : super(key: key);

  @override
  _PageAState createState() => _PageAState();
}

class _PageAState extends State<PageA> {
  var text = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    EventBus().on("update", (arg) {
      setState(() {
        text = arg;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PageA"),
      ),
      body: ListView(
        children: [
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "/pageB");
              },
              child: Text("跳转到B界面")),
          Text(text)
        ],
      ),
    );
  }
}
