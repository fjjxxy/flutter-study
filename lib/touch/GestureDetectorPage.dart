import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study/common/ContextKey.dart';
import 'package:flutter_study/utils/Toast.dart';

class GestureDetectorPage extends StatefulWidget {
  @override
  _GestureDetectorPageState createState() => _GestureDetectorPageState();
}

class _GestureDetectorPageState extends State<GestureDetectorPage> {
  var _top = 0.0;
  var _left = 0.0;
  var _screenWidth =
      MediaQuery.of(ContextKey.contextKey.currentContext).size.width;
  var _width = 0.0;
  var _height = 200.0;
  TapGestureRecognizer _gestureRecognizer = new TapGestureRecognizer();
  TapGestureRecognizer _gestureRecognizer2 = new TapGestureRecognizer();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _width = _screenWidth;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _gestureRecognizer?.dispose();
    _gestureRecognizer2?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("手势识别"),
      ),
      body: Column(
        children: [
          GestureDetector(
              child: Container(
                color: Colors.blue,
                alignment: Alignment.center,
                width: _width,
                height: _height,
                child: Text(
                  "手势识别",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              onTap: () {
                Toast.toast(context, msg: "点击！");
              },
              onDoubleTap: () {
                Toast.toast(context, msg: "双击");
              },
              onDoubleTapDown: (details) {
                Toast.toast(context, msg: "双击开始");
              },
              onLongPress: () {
                Toast.toast(context, msg: "长按");
              },
              onHorizontalDragUpdate: (details) {
                Toast.toast(context, msg: "水平滑动");
              },
              onScaleUpdate: (ScaleUpdateDetails details) {
                setState(() {
                  //缩放倍数在0.8到10倍之间
                  print(details.toString());
                  var width = _width * details.horizontalScale;
                  var height = _height * details.verticalScale;
                  _width = width > _screenWidth
                      ? _screenWidth
                      : width < 100
                          ? 100
                          : width;
                  _height = height > 200
                      ? 200
                      : height < 100
                          ? 100
                          : height;
                });
              }),
          Container(
            color: Colors.tealAccent,
            height: 100,
            child: Stack(
              children: [
                Positioned(
                    top: _top,
                    left: _left,
                    child: GestureDetector(
                      child: Chip(
                        label: Text("可进行拖拽"),
                        avatar: CircleAvatar(
                          child: Text("李"),
                        ),
                      ),
                      onPanUpdate: (details) {
                        setState(() {
                          _top += details.delta.dy;
                          _left += details.delta.dx;
                          if (_top < 0) {
                            _top = 0;
                          }
                          if (_left < 0) {
                            _left = 0;
                          }
                        });
                      },
                    ))
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text.rich(TextSpan(children: [
            TextSpan(text: "我已阅读并同意"),
            TextSpan(
                text: "《隐私政策》",
                style: TextStyle(
                    color: Colors.red,
                    decoration: TextDecoration.underline,
                    decorationStyle: TextDecorationStyle.solid),
                recognizer: _gestureRecognizer
                  ..onTap = () {
                    Toast.toast(context, msg: "点击隐私政策");
                  }),
            TextSpan(text: "及"),
            TextSpan(
                text: "《用户协议》",
                style: TextStyle(
                    color: Colors.red,
                    decoration: TextDecoration.underline,
                    decorationStyle: TextDecorationStyle.solid),
                recognizer: _gestureRecognizer2
                  ..onTap = () {
                    Toast.toast(context, msg: "点击用户协议");
                  })
          ]))
        ],
      ),
    );
  }
}
