
import 'package:flutter/material.dart';
import 'package:flutter_study/ComponentPage.dart';
import 'package:flutter_study/NavigationBarPage.dart';
import 'package:flutter_study/SharePrefrencePage.dart';
import 'package:flutter_study/appbar/TabControllerPage.dart';
import 'package:flutter_study/component/ButtonPage.dart';
import 'package:flutter_study/component/ChipPage.dart';
import 'package:flutter_study/component/DialogPage.dart';
import 'package:flutter_study/component/ImagePage.dart';
import 'package:flutter_study/component/ListViewPage.dart';
import 'package:flutter_study/component/ProgressBarPage.dart';
import 'package:flutter_study/component/RadioPage.dart';
import 'package:flutter_study/component/SwipePage.dart';
import 'package:flutter_study/component/TextFieldPage.dart';
import 'package:flutter_study/component/refresh/RefreshListViewPage.dart';
import 'package:flutter_study/eventbus/PageA.dart';
import 'package:flutter_study/eventbus/PageB.dart';
import 'package:flutter_study/file/PickImagePage.dart';
import 'package:flutter_study/http/DioPage.dart';
import 'package:flutter_study/http/HttpPage.dart';
import 'package:flutter_study/http/WebSocketPage.dart';
import 'package:flutter_study/pageview/GuidePage.dart';
import 'package:flutter_study/sqlite/SqlitePage.dart';
import 'package:flutter_study/touch/GestureDetectorPage.dart';
import 'package:flutter_study/touch/PointPage.dart';

import 'ForthPage.dart';
import 'MainPage.dart';
import 'SecondPage.dart';
import 'ServicePage.dart';
import 'ThirdPage.dart';
import 'appbar/AppBarDemoPage.dart';
import 'component/CheckBoxPage.dart';

//配置路由，以后所有的跳转的页面都可以再这里完成
final routes = {
  '/': (context) => GuidePage(),
  '/main': (context) => MainPage(), //配置根目录
  '/second': (context, {arguments}) => SecondPage(arguments: arguments),
  '/third': (context) => ThirdPage(),
  '/forth': (context, {arguments}) => ForthPage(arguments: arguments),
  '/tabs': (context, {arguments}) => NavigationBarPage(arguments: arguments),
  '/component': (context) => ComponentPage(),
  '/image': (context) => ImagePage(),
  '/listView': (context) => ListViewPage(),
  '/appbarPage': (context) => AppBarDemoPage(),
  '/tabController': (context) => TabControllerPage(),
  '/buttonPage': (context) => ButtonPage(),
  '/textFieldPage': (context) => TextFieldPage(),
  '/checkBoxPage': (context) => CheckBoxPage(),
  '/radioPage': (context) => RadioPage(),
  '/httpPage': (context) => HttpPage(),
  '/dioPage': (context) => DioPage(),
  '/sqlite': (context) => SqlitePage(),
  '/dialog': (context) => DialogPage(),
  '/chip': (context) => ChipPage(),
  '/refreshListView': (context) => RefreshListViewPage(),
  '/swipe': (context) => SwipePage(),
  '/progressBar': (context) => ProgressBarPage(),
  '/servicePage': (context) => ServicePage(),
  '/eventBus': (context) => PageA(),
  '/pageB': (context) => PageB(),
  '/webSocket': (context) => WebSocketPage(),
  '/openAlbum': (context) => PickImagePage(),
  '/PointPage': (context) => PointPage(),
  '/GestureDetectorPage': (context) => GestureDetectorPage(),
  '/SharePreference': (context) => SharePrefrencePage(),
};

//固定写法，理解不了就记住。
var onGenerateRoute = (RouteSettings settings) {
  //统一处理
  final String name = settings.name;
  final Function pageContentBuilder = routes[name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
      return route;
    } else {
      final Route route =
          MaterialPageRoute(builder: (context) => pageContentBuilder(context));
      return route;
    }
  }
};
