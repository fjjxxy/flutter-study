import 'package:flutter/material.dart';
import 'package:flutter_study/common/ContextKey.dart';

import 'Routes.dart';
import 'common/Global.dart';

// void main() => Global.init().then((value) => runApp(MyApp()));
void main() =>  runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //去掉debug图标
      //设置完命名路由不能只是热加载，要重新运行,真的气！
      //路由页面不能用MaterialApp包裹，不然没有返回键
      //跳转的时候构造方法中没有接收参数就别传参数，会报错
      initialRoute: "/",
      onGenerateRoute: onGenerateRoute,
      theme: ThemeData(primarySwatch: Colors.red),
      navigatorKey: ContextKey.contextKey,
    );
  }
}
