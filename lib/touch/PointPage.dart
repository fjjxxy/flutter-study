import 'package:flutter/material.dart';

class PointPage extends StatefulWidget {
  @override
  _PointPageState createState() => _PointPageState();
}

class _PointPageState extends State<PointPage> {
  //定义一个状态，保存当前指针位置
  PointerEvent _event;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("手指事件"),
      ),
      body: Listener(
        child: Container(
            alignment: Alignment.center,
            color: Colors.white,
            width: double.infinity,
            height: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("position:${_event?.position.toString()}",
                    style: TextStyle(color: Colors.black, fontSize: 20)),
                Text("delta:${_event?.delta.toString()}",
                    style: TextStyle(color: Colors.black, fontSize: 20)),
                Text("pressure:${_event?.pressure.toString()}",
                    style: TextStyle(color: Colors.black, fontSize: 20)),
                Text("kind:${_event?.runtimeType.toString()}",
                    style: TextStyle(color: Colors.black, fontSize: 20)),
              ],
            )),
        onPointerDown: (PointerDownEvent event) {
          setState(() {
            _event = event;
          });
        },
        onPointerMove: (PointerMoveEvent event) {
          setState(() {
            _event = event;
          });
        },
        onPointerUp: (event) {
          setState(() {
            _event = event;
          });
        },
      ),
    );
  }
}
