import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_study/utils/NumberUtils.dart';

class ProgressBarPage extends StatefulWidget {
  @override
  _ProgressBarPageState createState() => _ProgressBarPageState();
}

class _ProgressBarPageState extends State<ProgressBarPage>
    with SingleTickerProviderStateMixin {
  var value = 0.0;
  Timer time;
  AnimationController _animationController;

  @override
  void initState() {
    // TODO: implement initState
    // 动画执行时间
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 10));
    _animationController.forward();
    _animationController.addListener(() => setState(() => {}));
    time = Timer.periodic(Duration(milliseconds: 1000), (timer) {
      print("$value${value == 1.0}");
      if (value == 1.0) {
        setState(() {
          value = 0;
        });
      }
      setState(() {
        value = NumberUtils.addNumber(value, 0.1);
      });
      print("tick ${timer.tick}, timer isActive ${timer.isActive}");
    });
    super.initState();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    time.cancel();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("进度条"),
      ),
      body: ListView(
        children: [
          SizedBox(
            height: 20,
          ),
          LinearProgressIndicator(
            // value: 0.1,
            valueColor: AlwaysStoppedAnimation(Colors.red),
            backgroundColor: Colors.white,
          ),
          SizedBox(
            height: 20,
          ),
          LinearProgressIndicator(
            value: value,
            valueColor: AlwaysStoppedAnimation(Colors.red),
            backgroundColor: Colors.black54,
            minHeight: 10,
          ),
          SizedBox(
            height: 20,
          ),
          LinearProgressIndicator(
            backgroundColor: Colors.white,
            valueColor: ColorTween(begin: Colors.red, end: Colors.red)
                .animate(_animationController),
            value: _animationController.value,
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                strokeWidth: 15,
                valueColor: AlwaysStoppedAnimation(Colors.red),
                backgroundColor: Colors.black54,
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                value: value,
                valueColor: AlwaysStoppedAnimation(Colors.red),
                backgroundColor: Colors.black54,
              )
            ],
          ),
        ],
      ),
    );
  }
}
