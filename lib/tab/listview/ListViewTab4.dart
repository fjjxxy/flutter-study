import 'package:flutter/material.dart';
import 'package:flutter_study/data/Data.dart';

class ListViewTab4 extends StatefulWidget {
  @override
  _ListViewTab4State createState() => _ListViewTab4State();
}

class _ListViewTab4State extends State<ListViewTab4> {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context,index){
        return Container(
          height: 1,
          color: Colors.grey,
        );

      },
      itemCount: list.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text("${list[index]["name"]}"),
        );
      },
    );
  }
}
