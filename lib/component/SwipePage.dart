import 'package:flutter/material.dart';
import 'package:flutter_study/component/refresh/RefreshScrollView.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

class SwipePage extends StatefulWidget {
  @override
  _SwipePageState createState() => _SwipePageState();
}

class _SwipePageState extends State<SwipePage> {
  List<String> _imageList = [
    "https://img2.baidu.com/it/u=381105345,1838346956&fm=26&fmt=auto&gp=0.jpg",
    "https://img1.baidu.com/it/u=389248813,3199146309&fm=26&fmt=auto&gp=0.jpg",
    "https://img1.baidu.com/it/u=2988987662,2047918371&fm=26&fmt=auto&gp=0.jpg"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("轮播"),
      ),
      body: RefreshScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Container(
              width: double.infinity,
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Swiper(
                  itemCount: _imageList.length,
                  itemBuilder: (context, index) {
                    return Image.network(
                      _imageList[index],
                      fit: BoxFit.cover,
                    );
                  },
                  autoplay: true,
                  //自动轮播
                  onIndexChanged: (index) {},
                  //引起下标变化的监听
                  onTap: (index) {},
                  //点击轮播时调用
                  duration: 1000,
                  //切换时的动画时间
                  autoplayDelay: 2000,
                  //自动播放间隔毫秒数.
                  autoplayDisableOnInteraction: false,
                  loop: true,
                  //是否无限轮播
                  scrollDirection: Axis.horizontal,
                  //滚动方向
                  index: 0,
                  //初始下标位置
                  scale: 0.6,
                  //轮播图之间的间距
                  viewportFraction: 0.8,
                  //当前视窗比例，小于1时就会在屏幕内，可以看见旁边的轮播图
                  indicatorLayout: PageIndicatorLayout.COLOR,
                  pagination: new SwiperPagination(),
                  //底部指示器
                  control: new SwiperControl(), //左右箭头
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: double.infinity,
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Swiper(
                  itemCount: _imageList.length,
                  itemBuilder: (context, index) {
                    return Image.network(
                      _imageList[index],
                      fit: BoxFit.cover,
                    );
                  },
                  autoplay: true,
                  //自动轮播
                  onIndexChanged: (index) {},
                  //引起下标变化的监听
                  onTap: (index) {},
                  //点击轮播时调用
                  duration: 1000,
                  //切换时的动画时间
                  autoplayDelay: 2000,
                  //自动播放间隔毫秒数.
                  autoplayDisableOnInteraction: false,
                  loop: true,
                  //是否无限轮播
                  scrollDirection: Axis.horizontal,
                  //滚动方向
                  index: 0,
                  //初始下标位置
                  scale: 0.6,
                  //轮播图之间的间距
                  viewportFraction: 0.8,
                  //当前视窗比例，小于1时就会在屏幕内，可以看见旁边的轮播图
                  indicatorLayout: PageIndicatorLayout.COLOR,
                  pagination: new SwiperPagination(),
                  //底部指示器
                  layout: SwiperLayout.TINDER,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            buildVerticalSwipe(),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.only(top: 20),
              width: double.infinity,
              height: 250,
              child: AspectRatio(
                aspectRatio: 16 / 9,
                child: Swiper(
                  itemWidth: 300,
                  itemHeight: 200,
                  layout: SwiperLayout.CUSTOM,
                  customLayoutOption: new CustomLayoutOption(
                          startIndex: -1, stateCount: 3)
                      .addRotate([-45.0 / 180, 0.0, 45.0 / 180]).addTranslate([
                    new Offset(-370.0, -40.0),
                    new Offset(0.0, 0.0),
                    new Offset(370.0, -40.0)
                  ]),
                  autoplay: true,
                  duration: 2000,
                  itemBuilder: (context, index) {
                    return new Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                              image: NetworkImage(_imageList[index]),
                              fit: BoxFit.fill)),
                    );
                  },
                  itemCount: _imageList.length,
                  indicatorLayout: PageIndicatorLayout.COLOR,
                  pagination: SwiperPagination(
                      alignment: Alignment.bottomCenter,
/*                      builder: FractionPaginationBuilder(
                          activeColor: Colors.grey,
                          color: Colors.black,
                          fontSize: 20,
                          activeFontSize: 25)*/
                      builder: RectSwiperPaginationBuilder(
                          activeColor: Colors.red,
                          color: Colors.grey,
                          size: Size(20, 10),
                          activeSize: Size(20, 10))),
                  //底部指示器
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Container buildVerticalSwipe() {
    return Container(
      width: double.infinity,
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: Swiper(
          itemCount: _imageList.length,
          itemBuilder: (context, index) {
            return Image.network(
              _imageList[index],
              fit: BoxFit.cover,
            );
          },
          autoplay: true,
          //自动轮播
          onIndexChanged: (index) {},
          //引起下标变化的监听
          onTap: (index) {},
          //点击轮播时调用
          duration: 1000,
          //切换时的动画时间
          autoplayDelay: 2000,
          //自动播放间隔毫秒数.
          autoplayDisableOnInteraction: false,
          loop: true,
          //是否无限轮播
          scrollDirection: Axis.vertical,
          //滚动方向
          index: 0,
          //初始下标位置
          scale: 0.6,
          //轮播图之间的间距
          viewportFraction: 0.8,
          //当前视窗比例，小于1时就会在屏幕内，可以看见旁边的轮播图
          indicatorLayout: PageIndicatorLayout.SCALE,
          pagination: SwiperPagination(
              alignment: Alignment.bottomCenter,
              builder: DotSwiperPaginationBuilder(
                  color: Colors.grey,
                  activeColor: Colors.red,
                  size: 10.0,
                  activeSize: 20.0,
                  space: 5.0)

          ),
          //底部指示器
        ),
      ),
    );
  }
}
