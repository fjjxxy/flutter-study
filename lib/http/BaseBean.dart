
import 'package:flutter_study/generated/json/base/json_convert_content.dart';

class BaseBean<T> {
  T data;
  int errorCode;
  String errorMsg;

  BaseBean({this.data, this.errorCode, this.errorMsg});

  BaseBean.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null && json['data'] != 'null') {
      data = JsonConvert.fromJsonAsT<T>(json['data']);
    }
    errorCode = json['errorCode'];
    errorMsg = json['errorMsg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data;
    }
    data['errorCode'] = this.errorCode;
    data['errorMsg'] = this.errorMsg;
    print(data is Map);
    return data;
  }
}