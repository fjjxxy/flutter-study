import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PickImagePage extends StatefulWidget {
  @override
  _PickImagePageState createState() => _PickImagePageState();
}

class _PickImagePageState extends State<PickImagePage> {
  var _imagePath;
  var _videoPath = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("选择图片"),
      ),
      body: Column(
        children: [
          _imagePath != null
              ? Image.file(
                  new File(_imagePath),
                  width: 200,
                  height: 200,
                  fit: BoxFit.cover,
                )
              : SizedBox(),
          ElevatedButton(
              onPressed: () async {
                final pickedFile =
                    await ImagePicker().getImage(source: ImageSource.gallery);
                setState(() {
                  _imagePath = pickedFile.path;
                });
              },
              child: Text("选择图片(单张)")),
        ],
      ),
    );
  }
}
