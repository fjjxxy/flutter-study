import 'package:flutter/material.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Content(),
      ),
      appBar: AppBar(
        title: Text("第三个页面"),
      ),
    );
  }
}

class Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("这是第三个页面"),
        RaisedButton(
          onPressed: () {
            Navigator.pushReplacementNamed(context, "/forth", arguments: {
              "content":
                  "替换路由会加载下一个页面并让下一个页面替换当前的路由,\n即page1->page2->page3(这时候替换路由并跳转)->page4,这时候返回会回到page2,因为page3被替换了"
            });
          },
          child: Text("替换路由并跳转到第四个页面,返回到第二个页面"),
        ),
      ],
    );
  }
}
