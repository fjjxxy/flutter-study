import 'package:flutter/material.dart';
import 'package:flutter_study/data/Data.dart';

class ListViewTab2 extends StatefulWidget {
  @override
  _ListViewTab2State createState() => _ListViewTab2State();
}

class _ListViewTab2State extends State<ListViewTab2> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _getData(),
    );
  }
}
List<Widget> _getData() {
  List<Widget> widgetList = list.map((value) {
   if(value["id"]%2==0){
     return Container(
       height: 40,
       alignment: Alignment.center,
       decoration: BoxDecoration(borderRadius: BorderRadius.only(topRight: Radius.circular(10),bottomRight: Radius.circular(10)),border: Border.all(color: Colors.black)),
       margin: EdgeInsets.fromLTRB(0, 10, 40, 10),
       child: Text("${value["name"]}"),
     );
   }else{
     return Container(
       height: 40,
       alignment: Alignment.center,
       decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(10),bottomLeft: Radius.circular(10)),border: Border.all(color: Colors.black)),
       margin: EdgeInsets.fromLTRB(40, 10, 0, 10),
       child: Text("${value["name"]}"),
     );
   }
  }).toList();
  return widgetList;
}