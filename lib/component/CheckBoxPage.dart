import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study/utils/Toast.dart';

class CheckBoxPage extends StatefulWidget {
  @override
  _CheckBoxPageState createState() => _CheckBoxPageState();
}

class _CheckBoxPageState extends State<CheckBoxPage> {
  bool check1 = false, check2 = false, check3 = false;
  bool check4 = false,check5 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("复选框"),
      ),
      body: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: 10,
              ),
              Text("复选框1"),
              Checkbox(
                  activeColor: Colors.black,
                  value: this.check1,
                  onChanged: (value) {
                    setState(() {
                      this.check1 = value;
                    });
                    Toast.toast(context, msg: value ? "选中" : "未选中");
                  }),
              Text("复选框2"),
              Checkbox(
                  activeColor: Colors.black,
                  value: this.check2,
                  onChanged: (value) {
                    setState(() {
                      this.check2 = value;
                    });
                  }),
              Text("复选框3"),
              Checkbox(
                  activeColor: Colors.black,
                  value: this.check3,
                  onChanged: (value) {
                    setState(() {
                      this.check3 = value;
                    });
                  })
            ],
          ),
          SizedBox(
            height: 10,
          ),
          CheckboxListTile(
            value: this.check4,
            onChanged: (value) {
              setState(() {
                this.check4 = value;
              });
            },
            title: Text("复选框1"),
            subtitle: Text("复选框1"),
            secondary: Icon(Icons.home),
          ),
          CheckboxListTile(
            value: this.check5,
            onChanged: (value) {
              setState(() {
                this.check5 = value;
              });
            },
            title: Text("复选框2"),
            subtitle: Text("复选框2"),
            secondary: Icon(Icons.home),
          )
        ],
      ),
    );
  }
}
