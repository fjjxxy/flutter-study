import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study/MainPage.dart';

class GuidePage extends StatefulWidget {
  @override
  _GuidePageState createState() => _GuidePageState();
}

class _GuidePageState extends State<GuidePage> {
  var _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          PageView(
            scrollDirection: Axis.horizontal,
            reverse: false,
            onPageChanged: (index) {
              setState(() {
                _index = index;
              });
            },
            controller: PageController(
                initialPage: 0, keepPage: false, viewportFraction: 1),
            pageSnapping: true,
            physics: ClampingScrollPhysics(),
            children: [
              Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/image_guide_2.jpg"),
                        fit: BoxFit.fill)),
              ),
              Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/image_guide_1.jpg"),
                        fit: BoxFit.fill)),
              ),
              Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/image_guide_3.jpg"),
                        fit: BoxFit.fill)),
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Positioned(
                        bottom: 100,
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.pushAndRemoveUntil(context,
                                  new MaterialPageRoute(
                                      builder: (BuildContext context) {
                                return MainPage();
                              }), (route) => route == null);
                            },
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.white),
                                foregroundColor:
                                    MaterialStateProperty.all(Colors.black),
                                shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10))),
                                side: MaterialStateProperty.all(
                                    BorderSide(color: Colors.white))),
                            child: Text("开启Flutter学习之旅")))
                  ],
                ),
              )
            ],
          ),
          Positioned(
              bottom: 70,
              child: DotsIndicator(
                  mainAxisAlignment: MainAxisAlignment.center,
                  reversed: false,
                  dotsCount: 3,
                  position: _index.toDouble(),
                  decorator: DotsDecorator(
                    color: Colors.black87, // Inactive color
                    activeColor: Colors.redAccent,
                  )))
        ],
      ),
    );
  }
}
