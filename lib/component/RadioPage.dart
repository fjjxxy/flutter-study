import 'package:flutter/material.dart';

class RadioPage extends StatefulWidget {
  @override
  _RadioPageState createState() => _RadioPageState();
}

class _RadioPageState extends State<RadioPage> {
  var _groupValue = "男";
  var _groupValue2 = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("单选按钮"),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                children: [
                  Text("男"), //单选按钮的内容，但是不显示
                  Radio(
                      value: "男",
                      groupValue: this._groupValue,
                      onChanged: (value) {
                        setState(() {
                          this._groupValue = value;
                        });
                      }),
                  Text("女"),
                  Radio(
                      //波纹的颜色
                      overlayColor: MaterialStateProperty.all(Colors.blue),
                      activeColor: Colors.blue,
                      fillColor: MaterialStateProperty.all(Colors.black),
                      value: "女",
                      groupValue: this._groupValue,
                      onChanged: (value) {
                        setState(() {
                          this._groupValue = value;
                        });
                      })
                ],
              ),
              SizedBox(
                height: 10,
              ),
              RadioListTile(
                value: 1,
                groupValue: this._groupValue2,
                onChanged: _onChange,
                title: Text("单选框1"),
                subtitle: Text("单选框1"),
                secondary: Icon(Icons.help),
              ),
              SizedBox(
                height: 10,
              ),
              RadioListTile(
                value: 2,
                groupValue: this._groupValue2,
                onChanged: _onChange,
                title: Text("单选框2"),
                subtitle: Text("单选框2"),
                secondary: Icon(Icons.help),
              ),
              SizedBox(
                height: 10,
              ),
              RadioListTile(
                value: 3,
                groupValue: this._groupValue2,
                onChanged: _onChange,
                title: Text("单选框3"),
                subtitle: Text("单选框3"),
                secondary: Icon(Icons.help),
              ),
            ],
          ),
        ));
  }

  void _onChange(value) {
    setState(() {
      this._groupValue2 = value;
    });
  }
}
