import 'package:flutter/material.dart';
import 'package:flutter_study/component/CommonDialog.dart';
import 'package:flutter_study/utils/Color.dart';
import 'package:flutter_study/utils/Toast.dart';

class ChipPage extends StatefulWidget {
  @override
  _ChipPageState createState() => _ChipPageState();
}

class _ChipPageState extends State<ChipPage> {
  bool _isSelect = false;
  List<String> _list = ["标签1", "标签2", "标签3", "标签4", "标签5", "标签6", "标签7"];
  List<int> _indexList = [];
  int _selectIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Chip"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Wrap(
              spacing: 10,
              runSpacing: 10,
              children: [
                Chip(
                    label: Text("带头像的标签"),
                    avatar: CircleAvatar(
                      child: Text("刘"),
                      backgroundImage: NetworkImage(
                          "https://profile.csdnimg.cn/D/6/7/2_qq_40785165"),
                    )),
                Chip(label: Text("普通的标签")),
                Chip(
                  avatar: CircleAvatar(
                    child: Text(
                      "李",
                      style: TextStyle(color: ColorUtils.color_black),
                    ),
                    backgroundColor: ColorUtils.color_white,
                  ),
                  label: Text("带删除的标签"),
                  deleteButtonTooltipMessage: "提示:删除",
                  deleteIcon: Icon(Icons.close),
                  onDeleted: () {
                    Toast.toast(context, msg: "删除");
                  },
                  deleteIconColor: ColorUtils.color_red_f1091c,
                  useDeleteButtonTooltip: false,
                ),
                Chip(
                  label: Text("修改边框后的标签"),
                  labelPadding: EdgeInsets.all(10),
                  deleteButtonTooltipMessage: "提示:删除",
                  deleteIcon: Icon(Icons.close),
                  onDeleted: () {
                    Toast.toast(context, msg: "删除");
                  },
                  deleteIconColor: ColorUtils.color_red_f1091c,
                  useDeleteButtonTooltip: false,
                  //长按是否显示删除提示
                  side: BorderSide(color: ColorUtils.color_orange_main),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                ),
                Chip(
                  label: Text("带阴影的标签"),
                  labelPadding: EdgeInsets.all(10),
                  deleteButtonTooltipMessage: "提示:删除",
                  deleteIcon: Icon(Icons.close),
                  onDeleted: () {
                    Toast.toast(context, msg: "删除");
                  },
                  deleteIconColor: ColorUtils.color_red_f1091c,
                  useDeleteButtonTooltip: false,
                  //长按是否显示删除提示
                  elevation: 5,
                  shadowColor: ColorUtils.color_red_f1091c,
                ),
                ActionChip(
                    pressElevation: 10,
                    tooltip: "点击",
                    //长按提示
                    labelPadding: EdgeInsets.all(10),
                    avatar: CircleAvatar(
                      backgroundImage: NetworkImage(
                          "https://profile.csdnimg.cn/D/6/7/0_qq_40785165"),
                    ),
                    label: Container(
                        width: 120,
                        child: Row(children: [
                          Text("可点击的标签"),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(Icons.close)
                        ])),
                    onPressed: () {
                      Toast.toast(context, msg: "点击");
                    }),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: ColorUtils.color_orange_main)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("可选择的标签(ChoiceChip)"),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        spacing: 10,
                        runSpacing: 5,
                        children: getChoiceWrapChildren(),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: ColorUtils.color_orange_main)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("带有勾选标志的标签(FilterChip)"),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        spacing: 10,
                        runSpacing: 5,
                        children: getFilterWrapChildren(),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: ColorUtils.color_orange_main)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("同时具备点击&删除&选择&勾选标志的标签(InputChip)"),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        spacing: 10,
                        runSpacing: 5,
                        children: getInputWrapChildren(),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  List<Widget> getChoiceWrapChildren() {
    List<Widget> list = [];
    for (var i = 0; i < _list.length; i++) {
      list.add(ChoiceChip(
        avatar: CircleAvatar(
          backgroundImage: NetworkImage(
              "https://dss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3686156064,2328177349&fm=26&gp=0.jpg"),
        ),
        label: Text("${_list[i]}"),
        labelStyle: TextStyle(color: ColorUtils.color_white),
        selected: _selectIndex == i,
        onSelected: (select) {
          setState(() {
            _selectIndex = select ? i : null;
          });
        },
        selectedColor: ColorUtils.color_orange_main,
        disabledColor: ColorUtils.color_white,
        selectedShadowColor: ColorUtils.color_red_f1091c,
      ));
    }
    return list;
  }

  List<Widget> getFilterWrapChildren() {
    List<Widget> list = [];
    for (var i = 0; i < _list.length; i++) {
      list.add(FilterChip(
        showCheckmark: true,
        //是否显示勾选的标志
        checkmarkColor: ColorUtils.color_white,
        pressElevation: 0,
        clipBehavior: Clip.antiAlias,
        avatar: CircleAvatar(
          backgroundImage: NetworkImage(
              "https://dss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3686156064,2328177349&fm=26&gp=0.jpg"),
        ),
        label: Text("${_list[i]}"),
        labelStyle: TextStyle(color: ColorUtils.color_white),
        selected: _indexList.contains(i),
        onSelected: (select) {
          setState(() {
            if (select) {
              _indexList.add(i);
            } else {
              _indexList.remove(i);
            }
          });
        },
        selectedColor: ColorUtils.color_orange_main,
        disabledColor: ColorUtils.color_white,
        selectedShadowColor: ColorUtils.color_red_f1091c,
      ));
    }
    return list;
  }

//InputChip的onPressed和onSelected不能同时出现u
  List<Widget> getInputWrapChildren() {
    List<Widget> list = [];
    for (var i = 0; i < _list.length; i++) {
      list.add(InputChip(
        deleteIcon: Icon(Icons.close),
        onDeleted: () {
          showDialog(
              context: context,
              builder: (context) {
                return CommonDialog(
                    content: "确定删除吗?",
                    onConfirm: () {
                      setState(() {
                        _list.removeAt(i);
                      });
                      Toast.toast(context, msg: "已删除");
                      Navigator.pop(context);
                    },
                    onCancel: () {
                      Navigator.pop(context);
                    });
              });
        },
        deleteIconColor: ColorUtils.color_red_f1091c,
        useDeleteButtonTooltip: true,
        showCheckmark: true,
        //是否显示勾选的标志
        checkmarkColor: ColorUtils.color_white,
        pressElevation: 0,
        clipBehavior: Clip.antiAlias,
        avatar: CircleAvatar(
          backgroundImage: NetworkImage(
              "https://dss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3686156064,2328177349&fm=26&gp=0.jpg"),
        ),
        label: Text("${_list[i]}"),
        labelStyle: TextStyle(color: ColorUtils.color_white),
        /* onPressed: () {
          Toast.toast(context, msg: "以点击$i");
        },*/
        selected: _indexList.contains(i),
        onSelected: (select) {
          setState(() {
            if (select) {
              _indexList.add(i);
            } else {
              _indexList.remove(i);
            }
          });
        },
        selectedColor: ColorUtils.color_orange_main,
        disabledColor: ColorUtils.color_white,
        selectedShadowColor: ColorUtils.color_red_f1091c,
      ));
    }
    return list;
  }
}
