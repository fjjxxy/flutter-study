import 'package:flutter/material.dart';
import 'package:flutter_study/MainPage.dart';

class ForthPage extends StatefulWidget {
  final arguments;

  ForthPage({this.arguments});

  @override
  _ForthPageState createState() => _ForthPageState(arguments: this.arguments);
}

class _ForthPageState extends State<ForthPage> {
  var arguments;

  _ForthPageState({this.arguments});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Content(arguments: arguments),
      ),
      appBar: AppBar(
        title: Text("第四个页面"),
      ),
    );
  }
}

class Content extends StatelessWidget {
  var arguments;

  Content({this.arguments});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("这是第四个页面\n${arguments["content"]}"),
          RaisedButton(
            onPressed: () {
              //这种方式用构造方法传值
              // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=> MainPage()), (route) => false);
              //这种方式可以用arguments参数传值
              //第三个参数为true->可以返回，false->无法返回
              Navigator.pushNamedAndRemoveUntil(context, "/", (route) => false);
            },
            child: Text("移除之前的路由并返回"),
          ),
          RaisedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("普通返回"),
          )
        ],
      ),
    );
  }
}
