import 'package:dio/dio.dart';

import 'Api.dart';
import 'BaseBean.dart';

class HttpHelper {
  static Dio mDio;
  static BaseOptions options;
  static HttpHelper httpHelper;

  CancelToken cancelToken = CancelToken();

  static const String GET = 'get';
  static const String POST = 'post';
  static const String PUT = 'put';
  static const String PATCH = 'patch';
  static const String DELETE = 'delete';

  static HttpHelper get instance => getInstance();

  static Dio get dio => getDio();

  static HttpHelper getInstance() {
    if (null == httpHelper) httpHelper = HttpHelper();
    return httpHelper;
  }

  static Dio getDio() {
    options = BaseOptions(
      //请求基地址,可以包含子路径
      baseUrl: Api.BASE_URL,
      //连接服务器超时时间，单位是毫秒.
      connectTimeout: 10000,
      //2.x中为接收数据的最长时限
      receiveTimeout: 5000,
      //Http请求头.
      headers: {"token": ""},
      ///// 请求的Content-Type，默认值是"application/json; charset=utf-8".
      //   /// 如果您想以"application/x-www-form-urlencoded"格式编码请求数据,
      //   /// 可以设置此选项为 `Headers.formUrlEncodedContentType`,  这样[Dio]
      //   /// 就会自动编码请求体.
      contentType: Headers.jsonContentType,

      /// [responseType] 表示期望以那种格式(方式)接受响应数据。
      /// 目前 [ResponseType] 接受三种类型 `JSON`, `STREAM`, `PLAIN`.
      ///
      /// 默认值是 `JSON`, 当响应头中content-type为"application/json"时，dio 会自动将响应内容转化为json对象。
      /// 如果想以二进制方式接受响应数据，如下载一个二进制文件，那么可以使用 `STREAM`.
      ///
      /// 如果想以文本(字符串)格式接收响应数据，请使用 `PLAIN`.
      responseType: ResponseType.json,
    );
    mDio = Dio(options);
    mDio.interceptors.add(interceptorsWrapper());
    return mDio;
  }

  static InterceptorsWrapper interceptorsWrapper() {
    return InterceptorsWrapper(onRequest: (options, handler) {
      // Do something before request is sent
      print(options.path);
      print(options.queryParameters);
      print(options.headers.toString());
      return handler.next(options); //continue
      // 如果你想完成请求并返回一些自定义数据，你可以resolve一个Response对象 `handler.resolve(response)`。
      // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
      //
      // 如果你想终止请求并触发一个错误,你可以返回一个`DioError`对象,如`handler.reject(error)`，
      // 这样请求将被中止并触发异常，上层catchError会被调用。
    }, onResponse: (response, handler) {
      // Do something with response data
      return handler.next(response); // continue
      // 如果你想终止请求并触发一个错误,你可以 reject 一个`DioError`对象,如`handler.reject(error)`，
      // 这样请求将被中止并触发异常，上层catchError会被调用。
    }, onError: (DioError e, handler) {
      // Do something with response error
      return handler.next(e); //continue
      // 如果你想完成请求并返回一些自定义数据，可以resolve 一个`Response`,如`handler.resolve(response)`。
      // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
    });
  }

  ///Get请求
  void getHttp<T>(
    String url, {
    parameters,
    cancelToken,
    Function(BaseBean<T> t) onSuccess,
    Function(String error) onError,
  }) async {
    try {
      getResponse<T>(url,
          method: GET,
          cancelToken: cancelToken,
          parameters: parameters,
          onSuccess: onSuccess,
          onError: onError);
    } catch (e) {
      print(e);
    }
  }

  void postHttp<T>(
    String url, {
    String method,
    parameters,
    Function(BaseBean t) onSuccess,
    Function(String error) onError,
  }) async {
    ///定义请求参数
    parameters = parameters ?? Map<String, dynamic>();
    getResponse<T>(url,
        method: method ?? POST,
        parameters: parameters,
        onSuccess: onSuccess,
        onError: onError);
  }

  /*
   * 下载文件
   */
  downloadFile(urlPath, savePath, onReceiveProgress) async {
    Response response;
    try {
      response = await dio.download(urlPath, savePath,
          onReceiveProgress: onReceiveProgress);
    } on DioError catch (e) {
      formatError(e);
    }
    return response.data;
  }

  /*
   * 上传多个文件,key为files[]
   * v3.0.0 以后通过Formdata.fromMap()创建的Formdata,如果有文件数组，是默认会给key加上“[]”的
   */
  uploadFiles(String url, List<String> list, onSuccess, onError) async {
    var formData = FormData.fromMap({
      'files': list.map((e) {
        return MultipartFile.fromFileSync(e,
            filename: e.substring(e.indexOf("/")));
      })
    });
    getResponse(url,
        method: POST,
        parameters: formData,
        onSuccess: onSuccess,
        onError: onError);
  }

  uploadFiles2(String url, List<String> list, onSuccess, onError) async {
    var formData = FormData();
    list.map((e) {
      formData.files.add(MapEntry(
          'files',
          MultipartFile.fromFileSync(e,
              filename: e.substring(e.indexOf("/")))));
    });
    getResponse(url,
        method: POST,
        parameters: formData,
        onSuccess: onSuccess,
        onError: onError);
  }

  void getResponse<T>(
    String url, {
    String method,
    parameters,
    cancelToken,
    Function(BaseBean<T> t) onSuccess,
    Function(String error) onError,
  }) async {
    try {
      //这里指定response自动转成map,不指定的话有可能是String类型
      Response<Map<String, dynamic>> response;
      switch (method) {
        case GET:
          response = await dio.get(url,
              queryParameters: parameters ?? new Map<String, dynamic>(),
              cancelToken: cancelToken);
          break;
        case PUT:
          response = await dio.put(url,
              queryParameters: parameters ?? new Map<String, dynamic>(),
              cancelToken: cancelToken);
          break;
        case PATCH:
          response = await dio.patch(url,
              queryParameters: parameters ?? new Map<String, dynamic>(),
              cancelToken: cancelToken);
          break;
        case DELETE:
          response = await dio.delete(url,
              queryParameters: parameters ?? new Map<String, dynamic>(),
              cancelToken: cancelToken);
          break;
        default:
          response = await dio.post(url,
              data: parameters ?? new Map<String, dynamic>(),
              cancelToken: cancelToken);
          break;
      }
      //200代表网络请求成功
      if (response.statusCode == 200) {
        /// 将后台的data字段转成自己想要的数据/数据集,code根据后端实际返回进行判断访问结果
        BaseBean<T> bean = BaseBean.fromJson(response.data);
        if (bean.errorCode == 0 && onSuccess != null) {
          /// 返回BaseBean，里面的data是自己想要的数据
          onSuccess(bean);
        } else {
          onError(bean.errorMsg);
        }
      } else {
        throw Exception('${response.statusCode}+${response.statusMessage}');
      }
    } catch (e) {
      print(e.toString());
      onError(e.toString());
    }
  }

  void formatError(DioError e) {
    print(e.message);
  }

  /*
   * 取消请求
   * 同一个cancel token 可以用于多个请求，当一个cancel token取消时，所有使用该cancel token的请求都会被取消。
   */
  void cancelRequests(CancelToken token) {
    token.cancel("cancelled");
  }
}
