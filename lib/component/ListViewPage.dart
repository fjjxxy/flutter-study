import 'package:flutter/material.dart';
import 'package:flutter_study/tab/listview/GridViewTab.dart';
import 'package:flutter_study/tab/listview/ListViewTab1.dart';
import 'package:flutter_study/tab/listview/ListViewTab2.dart';
import 'package:flutter_study/tab/listview/ListViewTab3.dart';
import 'package:flutter_study/tab/listview/ListViewTab4.dart';

class ListViewPage extends StatefulWidget {
  @override
  _ListViewPageState createState() => _ListViewPageState();
}

class _ListViewPageState extends State<ListViewPage> {
  var _index = 0;

  List _pageList = [
    ListViewTab1(),
    ListViewTab2(),
    ListViewTab3(),
    ListViewTab4(),
    GridViewTab()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("列表组件"),
      ),
      body: this._pageList[_index],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _index,
          onTap: (index) {
            setState(() {
              _index = index;
            });
          },
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.list,
                  color: Colors.red,
                ),
                title: Text("垂直列表")),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.list,
                  color: Colors.red,
                ),
                title: Text("垂直列表2")),
            BottomNavigationBarItem(
                icon: Icon(Icons.list, color: Colors.red), title: Text("水平列表")),
            BottomNavigationBarItem(
                icon: Icon(Icons.list, color: Colors.red),
                title: Text("ListBuilder")),
            BottomNavigationBarItem(
                icon: Icon(Icons.grid_on, color: Colors.red),
                title: Text("网格列表")),
          ]),
    );
  }
}
