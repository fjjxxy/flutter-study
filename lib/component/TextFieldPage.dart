import 'package:flutter/material.dart';
import 'package:flutter_study/utils/Toast.dart';

class TextFieldPage extends StatefulWidget {
  @override
  _TextFieldPageState createState() => _TextFieldPageState();
}

class _TextFieldPageState extends State<TextFieldPage> {
  TextEditingController _text = new TextEditingController(text: "初始值");
  TextEditingController _text2 = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("文本框"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: buildColumn(),
        ),
      ),
    );
  }

  Column buildColumn() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextField(),
        buildSizedBox(),
        TextField(
          decoration: InputDecoration(hintText: "请输入用户名"),
        ),
        buildSizedBox(),
        TextField(
          decoration:
              InputDecoration(border: OutlineInputBorder(), hintText: "请输入用户名"),
        ),
        buildSizedBox(),
        TextField(
          maxLines: 4,
          minLines: 1,
          decoration:
              InputDecoration(border: OutlineInputBorder(), hintText: "请输入用户名"),
        ),
        buildSizedBox(),
        TextField(
          obscureText: true,
          decoration:
              InputDecoration(border: OutlineInputBorder(), hintText: "请输入密码"),
        ),
        buildSizedBox(),
        TextField(
          obscureText: true,
          decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: "请输入密码",
              labelStyle: TextStyle(color: Colors.black)),
        ),
        buildSizedBox(),
        TextField(
          controller: this._text2,
          decoration: InputDecoration(
            icon: Icon(Icons.account_balance),
            prefixIcon: Icon(Icons.account_balance),
            suffixIcon: IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                setState(() {
                  this._text2.text = "";
                });
              },
            ),
            labelText: "请输入用户名",
          ),
        ),
        buildSizedBox(),
        Row(
          children: [
            Expanded(
                child: TextField(
              controller: this._text,
            )),
            ElevatedButton(
                onPressed: () {
                  Toast.toast(context, msg: "提交的值为${this._text.text}");
                },
                child: Text("提交")),
          ],
        )
      ],
    );
  }

  SizedBox buildSizedBox() {
    return SizedBox(
      height: 10,
    );
  }
}
