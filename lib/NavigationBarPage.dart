import 'package:flutter/material.dart';
import 'package:flutter_study/tab/TabPage1.dart';
import 'package:flutter_study/tab/TabPage2.dart';
import 'package:flutter_study/tab/TabPage3.dart';
import 'package:flutter_study/tab/TabPage4.dart';

class NavigationBarPage extends StatefulWidget {
  final arguments;

  NavigationBarPage({this.arguments});

  @override
  _NavigationBarPageState createState() =>
      _NavigationBarPageState(arguments: this.arguments);
}

class _NavigationBarPageState extends State<NavigationBarPage> {
  Map arguments;
  var _index;

  _NavigationBarPageState({this.arguments}) {
    this._index = arguments!=null&&arguments["index"]!=null?arguments["index"]:0;
  }

  List _pageList = [TabPage1(), TabPage2(), TabPage3(), TabPage4()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("导航栏"),
      ),
      body: this._pageList[_index],
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: this._index,
          onTap: (int index) {
            print("点击位置===============$index");
            setState(() {
              _index = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                backgroundColor: Colors.deepPurpleAccent,
                icon: Icon(
                  Icons.search,
                  color: Colors.red,
                ),
                title: Text("标签1")),
            BottomNavigationBarItem(
                backgroundColor: Colors.deepOrangeAccent,
                icon: Icon(Icons.home, color: Colors.red),
                title: Text("标签2")),
            BottomNavigationBarItem(
                backgroundColor: Colors.pinkAccent,
                icon: Icon(Icons.tab, color: Colors.red),
                title: Text("标签3")),
            BottomNavigationBarItem(
                backgroundColor: Colors.teal,
                icon: Icon(Icons.favorite, color: Colors.red),
                title: Text("标签4")),
          ]),
    );
  }
}
