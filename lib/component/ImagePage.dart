import 'dart:ui';

import 'package:flutter/material.dart';

class ImagePage extends StatefulWidget {
  @override
  _ImagePageState createState() => _ImagePageState();
}

class _ImagePageState extends State<ImagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("图片组件"),
      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText("加载本地图片:"),
                  SizedBox(
                    height: 10,
                  ),
                  Image.asset("assets/images/icon_write_note.png",width: 100,height: 100,)
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText("加载网络图片:"),
                  SizedBox(
                    height: 10,
                  ),
                  Image.network(
                    "https://avatar.csdnimg.cn/D/6/7/3_qq_40785165_1608817824.jpg",
                    width: 100,
                    height: 100,
                    errorBuilder: (context, error, stackTrace) {
                      return Image.asset("assets/images/image_default.jpg");
                    },
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText("加载网络图片（混合颜色）:"),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      height: 100,
                      child: ListView(
                        shrinkWrap: true, //加上这个就不会因为高度报错了
                        scrollDirection: Axis.horizontal,
                        children: [
                          buildContainer(BlendMode.screen),
                          buildContainer(BlendMode.luminosity),
                          buildContainer(BlendMode.darken),
                          buildContainer(BlendMode.clear),
                          buildContainer(BlendMode.color),
                          buildContainer(BlendMode.colorBurn),
                          buildContainer(BlendMode.colorDodge),
                          buildContainer(BlendMode.difference),
                          buildContainer(BlendMode.dst),
                          buildContainer(BlendMode.dstATop),
                          buildContainer(BlendMode.dstIn),
                          buildContainer(BlendMode.dstOut),
                          buildContainer(BlendMode.dstOver),
                          buildContainer(BlendMode.exclusion),
                          buildContainer(BlendMode.hardLight),
                          buildContainer(BlendMode.hue),
                          buildContainer(BlendMode.lighten),
                          buildContainer(BlendMode.modulate),
                          buildContainer(BlendMode.multiply),
                          buildContainer(BlendMode.overlay),
                          buildContainer(BlendMode.plus),
                          buildContainer(BlendMode.saturation),
                          buildContainer(BlendMode.softLight),
                          buildContainer(BlendMode.src),
                          buildContainer(BlendMode.srcATop),
                          buildContainer(BlendMode.srcIn),
                          buildContainer(BlendMode.srcOut),
                          buildContainer(BlendMode.srcOver),
                        ],
                      ))
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText("加载网络图片（对齐方式）:"),
                  SizedBox(
                    height: 10,
                  ),
                  //Column嵌套listview必须要给listview找一个可以设置高度的容器包裹，不然会报错
                  Container(
                      height: 100,
                      child: ListView(
                        shrinkWrap: true, //加上这个就不会因为高度报错了
                        scrollDirection: Axis.horizontal,
                        children: [
                          buildContainer2(Alignment.topLeft),
                          buildContainer2(Alignment.topCenter),
                          buildContainer2(Alignment.topRight),
                          buildContainer2(Alignment.centerLeft),
                          buildContainer2(Alignment.center),
                          buildContainer2(Alignment.centerRight),
                          buildContainer2(Alignment.bottomLeft),
                          buildContainer2(Alignment.bottomCenter),
                          buildContainer2(Alignment.bottomRight),
                        ],
                      )),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText("加载网络图片（显示效果）:"),
                  SizedBox(
                    height: 10,
                  ),
                  //Column嵌套listview必须要给listview找一个可以设置高度的容器包裹，不然会报错
                  Container(
                      height: 100,
                      child: ListView(
                        shrinkWrap: true, //加上这个就不会因为高度报错了
                        scrollDirection: Axis.horizontal,
                        children: [
                          buildContainer3(BoxFit.contain),
                          //显示原比例，全图显示
                          buildContainer3(BoxFit.cover),
                          //充满容器，拉伸但是不变形
                          buildContainer3(BoxFit.fill),
                          //拉伸，充满容器，会变形
                          buildContainer3(BoxFit.fitHeight),
                          //高度充满为准
                          buildContainer3(BoxFit.fitWidth),
                          //宽度充满为准
                          buildContainer3(BoxFit.none),
                          //保持原图，显示中心位置，多出来的丢弃
                          buildContainer3(BoxFit.scaleDown),
                          //居中显示，如果需要，缩小源以确保源适合该框。如果会缩小图像，那么它与“contain”相同，否则它与“none”相同。
                        ],
                      )),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText("加载网络图片（填充效果）:"),
                  SizedBox(
                    height: 10,
                  ),
                  //Column嵌套listview必须要给listview找一个可以设置高度的容器包裹，不然会报错
                  Container(
                      height: 100,
                      child: ListView(
                        shrinkWrap: true, //加上这个就不会因为高度报错了
                        scrollDirection: Axis.horizontal,
                        children: [
                          buildContainer4(ImageRepeat.noRepeat),
                          buildContainer4(ImageRepeat.repeat),
                          buildContainer4(ImageRepeat.repeatX),
                          buildContainer4(ImageRepeat.repeatY),
                        ],
                      )),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText("加载网络图片（圆角效果）:"),
                  SizedBox(
                    height: 10,
                  ),
                  //Column嵌套listview必须要给listview找一个可以设置高度的容器包裹，不然会报错
                  Container(
                      height: 100,
                      child: ListView(
                        shrinkWrap: true, //加上这个就不会因为高度报错了
                        scrollDirection: Axis.horizontal,
                        children: [
                          buildRoundContainer(10),
                          buildRoundContainer2(10),
                        ],
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  ///文字组件
  Text buildText(text) =>
      Text(text, style: TextStyle(color: Colors.black, fontSize: 20));

  Container buildContainer(BlendMode blendMode) {
    return Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(color: Colors.grey),
        //图片如果要设置大小，不能直接放在container里面，要再用一个组件包裹，不然没办法设置大小
        child: Image.network(
          "https://avatar.csdnimg.cn/D/6/7/3_qq_40785165_1608817824.jpg",
          alignment: Alignment.center,
          color: Colors.red,
          colorBlendMode: blendMode,
        ));
  }

  Container buildContainer2(Alignment alignment) {
    return Container(
        width: 100,
        height: 100,
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        decoration: BoxDecoration(
            color: Colors.red, border: Border.all(color: Colors.black)),
        //图片如果要设置大小，不能直接放在container里面，要再用一个组件包裹，不然没办法设置大小
        child: Image.network(
          "https://avatar.csdnimg.cn/D/6/7/3_qq_40785165_1608817824.jpg",
          alignment: alignment,
          width: 80,
          height: 80,
        ));
  }

  Container buildContainer3(BoxFit boxFit) {
    return Container(
        width: 100,
        height: 100,
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        decoration: BoxDecoration(
            color: Colors.red, border: Border.all(color: Colors.black)),
        //图片如果要设置大小，不能直接放在container里面，要再用一个组件包裹，不然没办法设置大小
        child: Image.network(
          "https://img-blog.csdnimg.cn/img_convert/7c3b79791b1dd90edfa5a79cf12cae6a.png",
          fit: boxFit,
        ));
  }

  Container buildContainer4(ImageRepeat imageRepeat) {
    return Container(
        width: 100,
        height: 100,
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        decoration: BoxDecoration(
            color: Colors.red, border: Border.all(color: Colors.black)),
        //图片如果要设置大小，不能直接放在container里面，要再用一个组件包裹，不然没办法设置大小
        child: Image.network(
          "https://avatar.csdnimg.cn/D/6/7/3_qq_40785165_1608817824.jpg",
          repeat: imageRepeat,
        ));
  }

  Container buildRoundContainer(double radius) {
    return Container(
      width: 100,
      height: 100,
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://avatar.csdnimg.cn/D/6/7/3_qq_40785165_1608817824.jpg"),
            fit: BoxFit.cover,
          ),
          border: Border.all(color: Colors.black),
          borderRadius: BorderRadius.circular(radius)),
    );
  }

  Container buildRoundContainer2(double radius) {
    return Container(
      width: 100,
        height: 100,
        color: Colors.red,
        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: ClipOval(
          child: Image.network(
            "https://avatar.csdnimg.cn/D/6/7/3_qq_40785165_1608817824.jpg",
            fit: BoxFit.cover,
          ),
        ));
  }
}
