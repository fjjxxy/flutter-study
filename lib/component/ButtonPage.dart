import 'package:flutter/material.dart';
import 'package:flutter_study/utils/Toast.dart';

class ButtonPage extends StatefulWidget {
  @override
  _ButtonPageState createState() => _ButtonPageState();
}

class _ButtonPageState extends State<ButtonPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("按钮组件"),
      ),
      floatingActionButton: FloatingActionButton(
        foregroundColor: Colors.white,
        backgroundColor: Colors.red,
        elevation: 10,
        onPressed: () {},
        tooltip: "你好，flutter",
        child: Icon(Icons.add),
        mini: false,
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: [
            RaisedButton(
              onPressed: () {
                Toast.toast(context, msg: "点击按钮");
              },
              child: Text("RaisedButton(已弃用)"),
              elevation: 10,
              color: Colors.red,
              textColor: Colors.white,
            ),
            RaisedButton.icon(
              onPressed: () {},
              icon: Icon(Icons.search),
              label: Text("带图标的RaisedButton"),
              elevation: 20,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            ),
            ElevatedButton(
              onPressed: () {},
              child: Text("ElevatedButton(新版本替换RaisedButton的)"),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.grey)),
            ),
            ElevatedButton.icon(
              onPressed: () {},
              icon: Icon(Icons.search),
              label: Text("带图标的ElevatedButton"),
            ),
            FlatButton(
              onPressed: () {},
              child: Text("FlatButton(已弃用)"),
              textColor: Colors.white,
              color: Colors.red,
            ),
            FlatButton.icon(
                onPressed: () {},
                textColor: Colors.white,
                color: Colors.red,
                splashColor: Colors.greenAccent,
                icon: Icon(Icons.search),
                label: Text("带图标的FlatButton")),
            TextButton(
                onPressed: () {},
                child: Text("TextButton(新版本替换FlatButton)"),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.resolveWith(
                      (states) {
                        if (states.contains(MaterialState.focused) ||
                            states.contains(MaterialState.pressed)) {
                          return Colors.red;
                        }
                        return Colors.blue;
                      },
                    ),
                    //设置字体颜色的
                    foregroundColor: MaterialStateProperty.all(Colors.white))),
            TextButton.icon(
              icon: Icon(Icons.search),
              onPressed: () {},
              label: Text("带图标的TextButton(圆角&边框)"),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  //设置字体颜色的
                  foregroundColor: MaterialStateProperty.all(Colors.red),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
                  side:
                      MaterialStateProperty.all(BorderSide(color: Colors.red))),
            ),
            //自带边框的按钮
            OutlineButton(
              borderSide: BorderSide(
                  color: Colors.red, width: 1, style: BorderStyle.solid),
              onPressed: () {},
              textColor: Colors.black,
              child: Text("OutlineButton(已弃用)"),
            ),
            OutlinedButton(
                onPressed: () {},
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
                  side: MaterialStateProperty.all(
                      BorderSide(color: Colors.black)),
                ),
                child: Text("OutlinedButton(新版本替换OutlineButton)")),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ButtonBar(
                  children: [
                    IconButton(onPressed: () {}, icon: Icon(Icons.search)),
                    IconButton(onPressed: () {}, icon: Icon(Icons.add)),
                    IconButton(onPressed: () {}, icon: Icon(Icons.menu)),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
