import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppBarDemoPage extends StatefulWidget {
  @override
  _AppBarDemoPageState createState() => _AppBarDemoPageState();
}

class _AppBarDemoPageState extends State<AppBarDemoPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 8,
      child: Scaffold(
        appBar: AppBar(
          title: Text("自定义AppBar"),
          brightness: Brightness.light,
          backgroundColor: Colors.blue,
          centerTitle: false,
          leading: IconButton(
              //左边的图标按钮,会覆盖默认的返回按钮
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_ios)),
          actions: [
            //右边的图标按钮
            IconButton(onPressed: () {}, icon: Icon(Icons.search)),
            IconButton(onPressed: () {}, icon: Icon(Icons.settings)),
          ],
          bottom: TabBar(
              unselectedLabelStyle: TextStyle(fontSize: 10),
              labelStyle: TextStyle(fontSize: 20),
              unselectedLabelColor: Colors.white,
              labelColor: Colors.black,
              indicatorWeight: 2,
              //指示器粗细
              indicatorColor: Colors.black,
              //指示器颜色
              isScrollable: true,
              tabs: <Widget>[
                Tab(
                  text: "分类1",
                  icon: Icon(Icons.menu),
                ),
                Tab(
                  text: "分类2",
                ),
                Tab(
                  text: "分类3",
                ),
                Tab(
                  text: "分类4",
                ),
                Tab(
                  text: "分类5",
                ),
                Tab(
                  text: "分类6",
                ),
                Tab(
                  text: "分类7",
                ),
                Tab(
                  text: "分类8",
                ),
              ]),
        ),
        body: TabBarView(children: <Widget>[
          Center(
            child: RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, "/tabController");
              },
              child: Text("通过TabController创建顶部Tab切换"),
            ),
          ),
          Center(
            child: Text("页面2"),
          ),
          Center(
            child: Text("页面3"),
          ),
          Center(
            child: Text("页面4"),
          ),
          Center(
            child: Text("页面5"),
          ),
          Center(
            child: Text("页面6"),
          ),
          Center(
            child: Text("页面7"),
          ),
          Center(
            child: Text("页面8"),
          ),
        ]),
      ),
    );
  }
}
