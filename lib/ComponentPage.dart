import 'package:flutter/material.dart';

class ComponentPage extends StatefulWidget {
  @override
  _ComponentPageState createState() => _ComponentPageState();
}

class _ComponentPageState extends State<ComponentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("组件")),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: [
              RaisedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/image");
                }, //这个属性不能为空，不然背景颜色无效
                child: Text(
                  "图片",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/listView");
                }, //这个属性不能为空，不然背景颜色无效
                child: Text(
                  "列表",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/refreshListView");
                  },
                  child: Text("下拉刷新&上拉加载的列表")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/buttonPage");
                  },
                  child: Text("按钮组件")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/textFieldPage");
                  },
                  child: Text("文本框组件")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/checkBoxPage");
                  },
                  child: Text("复选框组件")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/radioPage");
                  },
                  child: Text("单选框组件")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/dialog");
                  },
                  child: Text("Dialog")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/chip");
                  },
                  child: Text("Chip标签")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/swipe");
                  },
                  child: Text("轮播组件")),
              ElevatedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/progressBar");
                  },
                  child: Text("进度条"))
            ],
          ),
        ),
      ),
    );
  }
}
