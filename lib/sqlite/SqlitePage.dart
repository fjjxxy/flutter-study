import 'package:flutter/material.dart';
import 'package:flutter_study/utils/Toast.dart';

import 'SqliteHelper.dart';
import 'note_bean_entity.dart';

class SqlitePage extends StatefulWidget {
  @override
  _SqlitePageState createState() => _SqlitePageState();
}

class _SqlitePageState extends State<SqlitePage> {
  SqliteHelper sqliteHelper;
  List<NoteBeanEntity> noteList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sqliteHelper = new SqliteHelper();
    getAllNote();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SQLite"),
        actions: [
          //右边的图标按钮
          buildIconButton(Icons.add, onPress: () {
            addNote();
          }),
          buildIconButton(Icons.refresh, onPress: () {
            getAllNote();
          }),
          buildIconButton(Icons.delete, onPress: () {
            deleteAll();
          }),
          new PopupMenuButton<String>(
            itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
              const PopupMenuItem(
                child: ListTile(
                  title: Text("批量操作"),
                  leading: Icon(Icons.batch_prediction),
                ),
                value: "A",
              ),
              const PopupMenuItem(
                child: ListTile(
                    //这里面就不能定义点击事件了，否则会和菜单的点击事件冲突
                    title: Text("更改第一条数据"),
                    leading: Icon(Icons.update)),
                value: "B",
              )
            ],
            onSelected: (String action) {
              switch (action) {
                case "A":
                  batchOperate();
                  break;
                case "B":
                  updateFirst();
                  break;
              }
            },
          ),
        ],
      ),
      body: ListView.builder(
          itemCount: noteList.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text("${noteList[index].title}"),
            );
          }),
    );
  }

  IconButton buildIconButton(IconData icons, {onPress}) {
    return IconButton(onPressed: onPress, icon: Icon(icons));
  }

  void addNote() async {
    await sqliteHelper.open();
    NoteBeanEntity noteBeanEntity = new NoteBeanEntity();
    noteBeanEntity.title = noteList.length.toString();
    noteBeanEntity.content = "234";
    noteBeanEntity.noteCode = "234";
    noteBeanEntity.addTime = "2021-03-02 00:00:00";
    noteBeanEntity.updateTime = "2021-03-02 00:00:00";
    await sqliteHelper.insert(noteBeanEntity);
    getAllNote();
    await sqliteHelper.close();
  }

  void getAllNote() async {
    await sqliteHelper.open();
    List<NoteBeanEntity> list = await sqliteHelper.getAllNote(1);
    print(list.length);
    setState(() {
      noteList.clear();
      noteList.addAll(list);
    });
  }

  void deleteAll() async {
    await sqliteHelper.open();
    await sqliteHelper.deleteAll();
    getAllNote();
  }

  void updateFirst() async {
    await sqliteHelper.open();
    if (noteList.length > 0) {
      noteList[0].title = "123456";
    }
    await sqliteHelper.update(noteList[0]);
    getAllNote();
  }

  void batchOperate() async {
    print("批量操作");
    await sqliteHelper.open();
    await sqliteHelper.batchOperate();
    getAllNote();
  }
}
