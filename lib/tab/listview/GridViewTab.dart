import 'package:flutter/material.dart';
import 'package:flutter_study/data/Data.dart';

class GridViewTab extends StatefulWidget {
  @override
  _GridViewTabState createState() => _GridViewTabState();
}

class _GridViewTabState extends State<GridViewTab> {
  @override
  Widget build(BuildContext context) {
    /*  return GridView.count(
      crossAxisCount: 3,///三列
      crossAxisSpacing: 10,//水平元素的间距
      mainAxisSpacing: 10,//垂直距离的间距
      padding: EdgeInsets.all(20),
      children: _getData(),
    );*/
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,///三列
          crossAxisSpacing: 10, //水平元素的间距
          mainAxisSpacing: 10,//垂直距离的间距
        ),
        itemCount: list.length,
        itemBuilder: (context, index) {
          return Container(
            height: 40,
            alignment: Alignment.center,
            color: Colors.red,
            child: Text(
              "${list[index]["name"]}",
              style: TextStyle(color: Colors.white),
            ),
          );
        });
  }
}

List _getData() {
  List widgetList = list.map((e) {
    return Container(
      height: 40,
      alignment: Alignment.center,
      color: Colors.red,
      child: Text(
        "${e["name"]}",
        style: TextStyle(color: Colors.white),
      ),
    );
  }).toList();
  return widgetList;
}
