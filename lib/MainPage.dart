import 'package:flutter/material.dart';
import 'package:flutter_study/NavigationBarPage.dart';
import 'SecondPage.dart';

class MainPage extends StatelessWidget {
  var imageUrl = "https://avatar.csdnimg.cn/D/6/7/3_qq_40785165_1608817824.jpg";
  var imageUrl1 =
      "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3686156064,2328177349&fm=26&gp=0.jpg";
  var imageUrl2 =
      "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2306696130,3636777462&fm=26&gp=0.jpg";
  var imageUrl3 =
      "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3898438129,1463014098&fm=26&gp=0.jpg";
  var imageUrl4 =
      "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1763004641,1180461983&fm=26&gp=0.jpg";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Flutter练习记录")),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: [
              RaisedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return SecondPage(
                      arguments: {"id": 1},
                    );
                  }));
                }, //这个属性不能为空，不然背景颜色无效
                child: Text(
                  "路由",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/tabs",
                      arguments: {"index": 2});
                }, //这个属性不能为空，不然背景颜色无效
                child: Text(
                  "导航栏",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/component");
                }, //这个属性不能为空，不然背景颜色无效
                child: Text(
                  "组件",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/appbarPage");
                }, //这个属性不能为空，不然背景颜色无效
                child: Text(
                  "自定义AppBar",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.red,
              ),
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/httpPage");
                },
                child: Text("Http网络请求"),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red),
                    foregroundColor: MaterialStateProperty.all(Colors.white)),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, "/dioPage");
                },
                child: Text("Dio网络请求"),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red),
                    foregroundColor: MaterialStateProperty.all(Colors.white)),
              ),
              buildTextButton(context, "/sqlite", "SQLite"),
              buildTextButton(context, "/servicePage", "原生与Flutter互相调用"),
              buildTextButton(context, "/eventBus", "事件总线"),
              buildTextButton(context, "/webSocket", "WebSocket"),
              buildTextButton(context, "/openAlbum", "打开相册"),
              buildTextButton(context, "/PointPage", "手指事件"),
              buildTextButton(context, "/GestureDetectorPage", "手势识别"),
              buildTextButton(context, "/SharePreference", "SharePreference存储"),
            ],
          ),
        ),
      ),
      drawer: Drawer(
        child: Column(
          children: [
            buildUserAccountsDrawerHeader(),
            buildListTile(context, Icons.home, "路由", "/second"),
            Divider(),
            buildListTile(context, Icons.settings, "导航栏", "/tabs"),
            Divider(),
            buildListTile(context, Icons.tab, "组件", "/component"),
          ],
        ),
      ),
      endDrawer: Drawer(
        child: Column(
          children: [
            buildDrawerHeader(),
            buildListTile(context, Icons.home, "路由", "/second"),
            Divider(),
            buildListTile(context, Icons.settings, "导航栏", "/tabs"),
            Divider(),
            buildListTile(context, Icons.tab, "组件", "/component"),
          ],
        ),
      ),
    );
  }

  TextButton buildTextButton(
      BuildContext context, String routeName, String title) {
    return TextButton(
      onPressed: () {
        Navigator.pushNamed(context, routeName);
      },
      child: Text(title),
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.red),
          foregroundColor: MaterialStateProperty.all(Colors.white)),
    );
  }

  UserAccountsDrawerHeader buildUserAccountsDrawerHeader() {
    return UserAccountsDrawerHeader(
      arrowColor: Colors.black,
      onDetailsPressed: () {},
      currentAccountPictureSize: Size.square(72),
      otherAccountsPicturesSize: Size.square(40),
      accountName: Text("小黑", style: TextStyle(color: Colors.black)),
      accountEmail: Text(
        "xiaohei@qq.com",
        style: TextStyle(color: Colors.black),
      ),
      //头像
      currentAccountPicture: CircleAvatar(
        backgroundImage: NetworkImage(imageUrl),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(imageUrl3), fit: BoxFit.cover)),
      //右上角的其他图标
      otherAccountsPictures: [
        CircleAvatar(
          backgroundImage: NetworkImage(imageUrl1),
        ),
        CircleAvatar(
          backgroundImage: NetworkImage(imageUrl2),
        )
      ],
    );
  }

  Widget buildDrawerHeader() {
    return Row(children: [
      Expanded(
          child: DrawerHeader(
        child: Container(
          child: Center(
            child: Column(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundImage: NetworkImage(imageUrl),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "小黑",
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.call),
                    SizedBox(
                      width: 10,
                    ),
                    Text("Tel:123456789")
                  ],
                )
              ],
            ),
          ),
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
              image: NetworkImage(this.imageUrl4), fit: BoxFit.cover),
        ),
      )),
    ]);
  }

  ListTile buildListTile(
      BuildContext context, IconData icon, String text, String route) {
    return ListTile(
      leading: CircleAvatar(
        child: Icon(icon),
      ),
      title: Text(text),
      onTap: () {
        Navigator.of(context).pop();
        Navigator.pushNamed(context, route);
      },
    );
  }
}
