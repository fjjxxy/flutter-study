import 'package:flutter/material.dart';

class TabPage2 extends StatefulWidget {
  @override
  _TabPage2State createState() => _TabPage2State();
}

class _TabPage2State extends State<TabPage2> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,//去掉默认的返回按钮
            title: Row(
              children: [
                Expanded(
                  child: TabBar(tabs: [
                    Tab(
                      text: "分类1",
                    ),
                    Tab(
                      child: Container(
                        child: Row(children: [
                          Icon(Icons.search),
                          Text("分类2")
                        ],),
                      ),
                    ),Tab(
                      text: "分类3",
                    )
                  ]),
                )
              ],
            ),
          ),
          body: TabBarView(children: [
            Center(
              child: Text("页面1"),
            ),
            Center(
              child: Text("页面2"),
            ), Center(
              child: Text("页面3"),
            )
          ]),
        ));
  }
}
